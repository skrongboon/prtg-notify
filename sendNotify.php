<?php
function sendNotify($imageAbsPath=null,$msg,$debug = null){
	date_default_timezone_set("Asia/Bangkok");
	$url 	= "https://notify-api.line.me/api/notify";
	$token = "zW5pfnCRUUMnkvzoLkDuEyReeyQ9it5wAzjxyMNpKsb";//compression bot
	if($debug == true){
		$token 	= "vGD8KlyNbhE05FRqYwMZTAFuAez74OXzESBP68VfY03";//MyLineBot
	}
	$date 	= new \DateTime();
	$date 	= date("Y-m-d, H:i:s");
	$message= "$msg \r\n@".$date;
	$data 	= array();
	// $imgPath = "C:/xampp/htdocs/gen_sentry_chart/loudness_2018-02-16.png";
	$data["message"] =	$message;
	if($imageAbsPath != null){
		$imgPath 			= $imageAbsPath;
		$imgFilename 		= $msg;
		$cFile  			= new CurlFile($imgPath,"image/png",$imgFilename);
		$data["imageFile"] 	= $cFile;
	}

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($ch,CURLOPT_URL,$url);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
	curl_setopt($ch, CURLOPT_TIMEOUT, 10);
	curl_setopt($ch, CURLOPT_SAFE_UPLOAD, true);

	$headers = [
		'Content-type:multipart/form-data',
	    'Authorization:Bearer '.$token
	];

	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	$server_output = curl_exec($ch);
	if($server_output === false)
	    {
	        echo "Error Number:".curl_errno($ch)."<br>";
	        echo "Error String:".curl_error($ch);
	    }
	// echo $server_output;
	curl_close ($ch);
	
}