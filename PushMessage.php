<?php
function pushMessage($imageAbsPath=null,$msg,$debug = null){
	date_default_timezone_set("Asia/Bangkok");
	$url 	= "https://api.line.me/v2/bot/message/push";
	//$token = "zW5pfnCRUUMnkvzoLkDuEyReeyQ9it5wAzjxyMNpKsb";//compression bot
	if($debug == true){
		$token 	= "vGD8KlyNbhE05FRqYwMZTAFuAez74OXzESBP68VfY03";//MyLineBot
    }
    $token = "GCkQz6mDO5f64dNkMNaIQeaT0zu2kM0uQvZ55VERhcUqnsgvxTQUeYHaRFhPMiyQlR/40vdO2OkTn7ARGtqetg/uaj9pElPRKdDpePiaqvxbTtJOo9xbKBjDPWkfsaxQLzaflsR+zT3rrzujuB355QdB04t89/1O/w1cDnyilFU=";
    $channelUserId = "U29e00f58918061c6dbaddf126610d429";
	$date 	= new \DateTime();
	$date 	= date("Y-m-d, H:i:s");
	$message= "$msg \r\n@".$date;
	$data 	= array();
    // $imgPath = "C:/xampp/htdocs/gen_sentry_chart/loudness_2018-02-16.png";
    // PRTG return object
    $prtg_obj = json_decode($msg,1);

    // Flex message structure
    $flex_main   = array(
        "to"=>$channelUserId,
        "messages"=>array()
    ); 
    $flex_message = array(
        "type"=>"flex",
        "altText"=>"GROUP:".$prtg_obj["group"]."\nSTATUS: ".$prtg_obj["message"]
                    ."\nDEVICE: ".$prtg_obj["device"]
                    ."\nSENSOR: ".$prtg_obj["sensorname"]."=".$prtg_obj["lastvalue"],
        "contents"=>array(
            "type"=>"bubble",
            "styles"=>array(
                "header"=>array(
                   "backgroundColor"=>($prtg_obj['message']=="OK")?"#6CD26C":"#FD8692"
                ),
                "body"=>array(
                    "separator"=>true
                )
            ),
            "header"=>null,
            "body"=>null
        )
    );

    $header = array(
        "type"=>"box",
        "layout"=>"vertical",
        "contents"=>array(array(
            "type"=>"text",
            "text"=>($prtg_obj['message']=="OK")?"UP":"DOWN",
            "size"=>"md",
            "align"=>"center",
            "weight"=>"bold",

        ))
    );
    $body   = array(
        "type"=>"box",
        "layout"=>"vertical",
        "contents"=>array()
    );
    $body_content_groupname = array(
        "type"=>"box",
        "layout"=>"horizontal",
        "contents"=>array(
            array("type"=>"text","text"=>"GROUP","wrap"=>true),
            array("type"=>"text","text"=>$prtg_obj["group"],"wrap"=>true),
        ),
        
    );
    $body_content_device = array(
        "type"=>"box",
        "layout"=>"horizontal",
        "contents"=>array(
            array("type"=>"text","text"=>"DEVICE","wrap"=>true),
            array("type"=>"text","text"=>$prtg_obj["device"],"wrap"=>true),
        ),
    );
    $body_content_sensor = array(
        "type"=>"box",
        "layout"=>"horizontal",
        "contents"=>array(
            array("type"=>"text","text"=>"SENSOR","wrap"=>true),
            array("type"=>"text","text"=>$prtg_obj["sensorname"]." : ".$prtg_obj['lastvalue'],"wrap"=>true),
        ),
    );

    $body["contents"][] = $body_content_groupname;
    $body["contents"][] = array("type"=>"separator");
    $body["contents"][] = $body_content_device;
    $body["contents"][] = array("type"=>"separator");
    $body["contents"][] = $body_content_sensor;

    $flex_message["contents"]["header"]=$header;
    $flex_message["contents"]["body"]=$body;

    $flex_main["messages"][] = $flex_message;

    $flex_json =  json_encode($flex_main);
    echo $flex_json;

    $data =	$flex_json;
	if($imageAbsPath != null){
		$imgPath 			= $imageAbsPath;
		$imgFilename 		= $msg;
		$cFile  			= new CurlFile($imgPath,"image/png",$imgFilename);
		$data["imageFile"] 	= $cFile;
	}

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($ch,CURLOPT_URL,$url);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
	curl_setopt($ch, CURLOPT_TIMEOUT, 10);
	curl_setopt($ch, CURLOPT_SAFE_UPLOAD, true);

	$headers = [
		'Content-type:application/json',
	    'Authorization:Bearer '.$token
	];

	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    $server_output = curl_exec($ch);
    var_dump($server_output);
	if($server_output === false)
	    {
	        echo "Error Number:".curl_errno($ch)."<br>";
	        echo "Error String:".curl_error($ch);
	    }
	// echo $server_output;
	curl_close ($ch);
	
}