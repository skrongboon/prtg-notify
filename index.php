<?php
date_default_timezone_set("Asia/Bangkok");
include_once "PushMessage.php";
$allData = json_encode($_POST);
$logMessage = date("Y-m-d H:i:s")."  | ".$allData."\n";
file_put_contents("./access.log",$logMessage,FILE_APPEND);
pushMessage(null,$allData,true);